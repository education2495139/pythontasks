print('example1:')
x,y = 5,3
if y > 0.5:
    x = 10
    y = 3 * y
print(x, y, x * y, sep = ' ** ', end = '\n')

print('example2:')
X = ' '
if X == 1:
 print('один')
elif X == 2:
 print('два')
elif X == ' ':
 print('пусто')
else: print(' ??? ')

print('example3:')
a,b,c = 1, 2, 3
if (a == 1 and
b == 2 and
c == 3):
    print('x =', x)

print('example4:')
#if y>3: x=5
#else: x=3
x=5 if y>3 else 3

print('example5:')
x = 1
while x <= 5:
    print(x)
    x = x + 1

print('example6:')
x = 5
while True: # условие выполнено всегда
    x = x + 1
    print(x, end = ' ')
    if x == 10: break # выход из цикла
print()

print('\nexample7:')
print(range(2, 12))

print('\nexample8:')
print(range(2, 12, 3))

print('\nexample9:')
print(range(11, 2, -2))

print('\nexample10:')
X,Y,N = 1,1,20
for i in range(N + 1): print(i, X); X,Y = Y, X+Y

print('\nexample11:')
Text = "Py*t**ho**n"
for Z in Text:
    if Z == '*': continue
    print(Z, end = '')
print()

print('\nexample12:')
for X in (1.5, 2.5, 3, 2, 8.3):
    if X < 0: break
else: X = "все числа положительны"
print(X)
