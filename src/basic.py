print('example1:')
print('herzen university')
print(2**100)

print('example2:')
a=b=1
print(a,b)

print('example3:')
a,b=1,2
print(a,b)

print('example4:')
a,b=b,a
print(a,b)

print('example5:')
X,Y = 3,5
X,Y = Y, X*Y
print(X,Y)

print('example6:')
a,b = 15,(1,2,3)
print(a, b)

print('example7:')
a= 'abcde'
print(a)

print('example8:')
a, *b = 'abcde'
print(a)
print(b)

print('example9:')
x = 3; y = 5
print(x, y, x*y, sep = ' *** ', end = '\n')

print('example10:')
x = 'herzen'; y = 123; z = 'python'
print(x, y, z)
print()
print(x, y, z, sep = '', end = '')
print(x, y, z, sep = ', ')

print('example11:')
#a = input('Введите а: ')
#b = int(a)
#b = float(a)
#print(a, b)

print('example12:')
print(7//2)

print('example13:')
print(3*'abc')

print('example14:')
print(2 ** 8)

print('example15:')
print(pow(2, 0.5))

print('example16:')
x = 21
z = bin(x)
print(x, '=', str(z), '=', z)
z = 0b111
print(str(z), '=', z)

print('example17:')
X = complex(3, 5); Y = 1 + 2j
print(X, '  ', Y)
print(X / Y)
print(X ** 2)
print((-1) ** 0.5)

print('example17:')
Z=(1+2j)
print(Z.real)
print(Z.imag)
print(Z.conjugate())
print(abs(3 + 4j))
