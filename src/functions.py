print('\nexample1:')
def a_funct():
    print("Herzen University")
a_funct() #вызов функции
a_funct() #вызов функции

print('\nexample2:')
def fff(a, b):
    return a + 2 * b
print(fff(3, 6)) # напечатает 15
print(fff(b = 3, a = 2)) # напечатает 8
print(fff('aaa', 'bbb'))

print('\nexample3:')
def fff(a,b):
    return (a, b, a + 2 * b)
print(fff(3, 6))

print('\nexample4:')
def fff(a = 1, b = 2):
    return a + b
print(fff(b = 4, a = 5)) # напечатает 9
print(fff()) # напечатает 3
print(fff(3)) # напечатает 5

print('\nexample5:')
def mixed_arg(a, x = 2, y = 3):
    print(a + x + y)
mixed_arg(1) # напечатает 6
mixed_arg(1,5) # напечатает 9

print('\nexample6:')
def fff(a, b):
    x = 10
    return x + a + 2 * b
print(fff(3, 6)) # напечатает 25

print('\nexample7:')
def fff(a, b):
    return x + a + 2 * b
x = 20
print(fff(3, 6)) # напечатает 35

print('\nexample8:')
a = 1
def bbb():
    global a
    a = 25
print(a) # 1
bbb()
print(a) # 25

print('\nexample9:')
def fff(a, b):
    global x
    y = x + a + 2 * b
    x = 100
    return y
x = 20
print(fff(3, 6)) #выведет 35
print(x) #выведет 100

print('\nexample10:')
def Fib(n):
    if n < 2: return 1
    return Fib(n - 1) + Fib(n - 2)
print(Fib(5)) # результат равен 8

print('\nexample11:')
def fun(): pass
print(fun())

