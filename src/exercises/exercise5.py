# Решить с помощью функций модуля numpy.linalg систему
# линейных алгебраических уравнений (СЛАУ). 
# Получить решение разными способами: методом Крамера; 
# методом обратной матрицы; с помощью функции linalg.solve. 
# Выполнить проверку найденного решения.

# Вариант 5:

import numpy

A = [
    [-0.2, 1.6, -0.1],
    [-0.3, 0.1, -1.5],
    [1.2, -0.2, 0.3],
    ]
B = [0.3, 0.4, -0.6]

# Функция решения СЛАУ методом Крамера:
def Kramar(A, B):
    m = len(A)
    op = numpy.linalg.det(A)
    r = list()
    for i in range(m):
        VM = numpy.copy(A)
        VM[:, i] = B
        r.append(numpy.linalg.det(VM) / op)
    return r

# Функция решения СЛАУ методом обратной матрицы:
def Reverse(A, B):
    reverse_A = numpy.linalg.inv(A)
    return numpy.matmul(reverse_A, B)

# Результаты
print('\nМетодом Крамера:\n', Kramar(A, B))
print('\nМетодом обратной матрицы:\n', Reverse(A, B))
print('\nМетодом linalg.solve:\n', numpy.linalg.solve(A, B))
