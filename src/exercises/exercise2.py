# Проверить формулу суммирования ряда. Вычислить набор
# значений функции и сравнить с суммой ряда при тех же аргументах.
# При вычислении n-ного члена ряда рекуррентно выражать его через
# предыдущий (с номером n–1).

# Вариант 5:

from math import factorial, exp

def ch(x):
    return (exp(x) + exp(-1 * x)) / 2

def sum_series(x, n):
    sum = 0
    for n in range(0, n):
        current = (x ** (2 * n)) / factorial(2 * n)
        sum = sum + current
    return sum

for k in range(1, 10):
    (print('При x =', k,
           ' | Сумма ряда:', round(sum_series(k, 100), 7),
           '\t | ch(x) =', round(ch(k), 7)))
