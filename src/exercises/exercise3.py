# Составить программу, для решения задачи по обработке
# данных одномерного массива. Количество элементов массива задать
# самостоятельно. Элементы массива задать с помощью генератора случайных чисел. 

# Вариант 5:
# Дана последовательность действительных чисел a1, a2, ..., аn.
# Выяснить, будет ли она возрастающей.

from random import randint

def array_init(n):
    return [randint(1, 100) for x in range(0, n)]

def check_increasing_array(a):
    prev_element = 0
    for k in a:
        if k > prev_element:
            prev_element = k
        else:
            return False
    return True

array = array_init(3)
print('Последовательность: \n', array)
if check_increasing_array(array):
    print('Является возрастающей.')
else:
    print('Не является возрастающей.')
