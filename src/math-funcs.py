import math
#import math as std-math

print('\nexample1:')
print(math.pi)

print('\nexample2:')
from math import pi,e
print(pi)
print(e)

print('\nexample3:')
#help(math)
help(math.sin)

print('\nexample4:')
# возведение 2 в степень 3
n = math.pow(2, 3)
print(n) #ответ 8
# косинус 60 градусов
x = math.cos(math.radians(60))
print(x)

print('\nexample5:')
import cmath
print(cmath.sqrt(-1))
print(cmath.exp(1j*cmath.pi/2))

print('\nexample6:')
import random
X=random.random()
print(X);

print('\nexample7:')
A = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(random.sample(A, 5))

